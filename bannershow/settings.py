from django.conf import settings
import os
gettext = lambda s: s

BANNERSHOW_MEDIA = getattr(settings, 'BANNERSHOW_MEDIA', "bannershow")

BANNERSHOW_EFFECTS = getattr(settings, 'BANNERSHOW_EFFECTS', (
        'fade',
        'slideFromLeft',
        'slideFromRight',
        'slideFromTop',
        'slideFromBottom',
        'topBottomDroppingStripes',
        'topBottomDroppingReverseStripes',
        'bottomTopDroppingStripes',
        'bottomTopDroppingReverseStripes',
        'asynchronousDroppingStripes',
        'leftRightFadingStripes',
        'leftRightFadingReverseStripes',
        'topBottomDiagonalBlocks',
        'topBottomDiagonalReverseBlocks',
        'randomBlocks'
    )
)
