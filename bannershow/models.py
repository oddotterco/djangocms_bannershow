#       models.py
#
#       Copyright 2011 Garth Johnson <growlf@biocede.com>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from django.utils.translation import ugettext, ugettext_lazy as _
from django.db.models import Model, BooleanField, PositiveIntegerField, CharField, ManyToManyField, FloatField, IntegerField
from cms.models import CMSPlugin
from filer.fields.image import FilerImageField
from django.contrib.staticfiles.finders import find as staticfiles_find
import os
from settings import BANNERSHOW_EFFECTS, BANNERSHOW_MEDIA
from cms.models.fields import PageField


EFFECT_CHOICES = ((e, e) for e in BANNERSHOW_EFFECTS)


def find_skins(layout):
    skin_root = os.path.join(BANNERSHOW_MEDIA, layout, "skins")
    skindirs = staticfiles_find(skin_root, all=True)
    if skindirs:
        for skindir in skindirs:
            for skin in os.listdir(skindir):
                if os.path.isdir(os.path.join(skindir, skin)):
                    yield skin



class FeatureImage(Model):
    image = FilerImageField()
    link = PageField(null=True, blank=True)
    caption = CharField(_('caption'), max_length=255, null=True, blank=True)

    def __unicode__(self):
        return unicode(self.image)


class BSCMSPlugin(CMSPlugin):

    title = CharField(_('title'), max_length=255, null=True, blank=True)
    width = PositiveIntegerField(_('width'), default=1100,
        help_text=_("Width of the plugin (px)"))
    height = PositiveIntegerField(_('height'), default=380,
        help_text=_("Height of the plugin (px)"))
    images = ManyToManyField(FeatureImage, null=True, blank=True,
        help_text=_("Add images or select from this list that you want to show in the slider"))
    animationTime = FloatField(_('animation time'), default=0.8,
        help_text=_("Animation transition speed in seconds (smaller is quicker)"))
    autoPlay = PositiveIntegerField(_('autoPlay'), default=3,
        help_text=_("Animation pause in seconds (larger is longer)"))
    arrows = BooleanField(_('arrows'), default=True,
        help_text=_('Arrow buttons for navigation'))
    resizeImages = BooleanField(_('resize images'), default=True,
        help_text=_('Whether the bannershow should use image resizing instead of image cropping'))
    pause_on_hover = BooleanField(_('pause on mouse hover'), default=True)
    responsive = BooleanField(_('responsive'), default=True,
        help_text=_('Whether the bannershow should responsive'))
    responsiveRelativeToBrowser = BooleanField(_('browser responsive'), default=False,
        help_text=_('Whether the bannershow should responsive relative to the browser (auto re-flow on resize)'))
    enableTouchScreen = BooleanField(_('touch'), default=True,
        help_text=_('Whether the bannershow should responsive to touch screen devices'))
    showCircleTimer = BooleanField(_('show timer'), default=False,
        help_text=_('Whether to show the circle timer indicator'))

    def __unicode__(self):
        if self.title:
            return self.title
        return unicode("bannershow_plugin_%d" % self.id)

    def copy_relations(self, oldinstance):
        self.images = oldinstance.images.all()

    def thumb_size(self):
        width = int(self.width * 0.72)
        height = int(self.height * 0.75)
        return "%dx%d" % (width, height)

    @property
    def autoHideBottomNav(self):
        return True

    @property
    def showOnInitNavArrows(self):
        return self.arrows

    @property
    def showOnInitBottomNav(self):
        return self.arrows


class BannerRotatorPlugin(BSCMSPlugin):
    SKINS = ((t, t) for t in set(find_skins("bannerRotator")))
    skin = CharField(_('skin'), choices=SKINS, max_length=50)
    numberOfVisibleItems = PositiveIntegerField(_('Visible Items'), default=5,
        help_text=_("The number of items to be visible at a time (should be an odd number)"))
    elementsHorizontalSpacing = IntegerField(_('Horizontal spacing'), default=38,
        help_text=_("Horizontal offset for each element (in px)"))
    elementsVerticalSpacing = IntegerField(_('Vertical spacing'), default=110,
        help_text=_("Vertical offset for each element (in px)"))
    verticalAdjustment = IntegerField(_('Vertical adjustment'), default=0,
        help_text=_("Vertical adjustment for the carousel (in px)"))
    nextPrevMarginTop = IntegerField(_('Controls adjustment'), default=0,
        help_text=_("Vertical adjustment from top of slideshow area for the next and previous button controls (in px)"))
    playMovieMarginTop = IntegerField(_('Movie top'), default=0,
        help_text=_("Vertical adjustment from top of any videos in the slideshow (in px)"))
    bottomNavMarginBottom = IntegerField(_('Selector adjustment'), default=0,
        help_text=_("Vertical adjustment from bottom of slideshow area for the slide selector controls (in px)"))

    def __unicode__(self):
        if self.title:
            return self.title
        return unicode("banner_rotator_%d" % self.id)


class CarouselPlugin(BSCMSPlugin):

    SKINS = ((t, t) for t in set(find_skins("carousel")))
    skin = CharField(_('skin'), choices=SKINS, max_length=50)
    numberOfVisibleItems = PositiveIntegerField(_('Visible Items'), default=5,
        help_text=_("The number of items to be visible at a time (should be an odd number)"))
    elementsHorizontalSpacing = IntegerField(_('Horizontal spacing'), default=38,
        help_text=_("Horizontal offset for each element (in px)"))
    elementsVerticalSpacing = IntegerField(_('Vertical spacing'), default=110,
        help_text=_("Vertical offset for each element (in px)"))
    verticalAdjustment = IntegerField(_('Vertical adjustment'), default=0,
        help_text=_("Vertical adjustment for the carousel (in px)"))
    nextPrevMarginTop = IntegerField(_('Controls adjustment'), default=0,
        help_text=_("Vertical adjustment from top of slideshow area for the next and previous button controls (in px)"))
    playMovieMarginTop = IntegerField(_('Movie top'), default=0,
        help_text=_("Vertical adjustment from top of any videos in the slideshow (in px)"))
    bottomNavMarginBottom = IntegerField(_('Selector adjustment'), default=0,
        help_text=_("Vertical adjustment from bottom of slideshow area for the slide selector controls (in px)"))

    def __unicode__(self):
        if self.title:
            return self.title
        return unicode("bannershow_carousel_%d" % self.id)
