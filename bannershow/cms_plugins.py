from cms.plugin_base import CMSPluginBase
from django.utils.translation import ugettext_lazy as _
from cms.plugin_pool import plugin_pool
from .models import BannerRotatorPlugin as BannerRotatorPluginModel
from .models import CarouselPlugin as CarouselPluginModel

class BannerRotatorPlugin(CMSPluginBase):
    """
    Plugin to render a feature slider
    """
    model = BannerRotatorPluginModel
    name = _("Banner Rotator")
    render_template = "bannershow/bannerRotator.html"
    text_enabled = True
    admin_preview = False
    module = "Bannershow"
    fieldsets = (
        (_('General'), {
            'fields': (
                'title',
                'images',
                ('width', 'height', ),
                'skin',
                ('animationTime', 'autoPlay', ),
            ),
        }),
        (_('Display'), {
            'fields': (
                'resizeImages',
                ('responsive', 'responsiveRelativeToBrowser', ),
                'numberOfVisibleItems',
                'elementsHorizontalSpacing',
                'elementsVerticalSpacing',
                'verticalAdjustment',
                'playMovieMarginTop',
            ),
        }),
        (_('Controls'), {
            'fields': (
                'enableTouchScreen',
                'showCircleTimer',
                'arrows',
                'pause_on_hover',
                'nextPrevMarginTop',
                'bottomNavMarginBottom',
            ),
        }),
    )

    def render(self, context, instance, placeholder):
        images = instance.images.all()
        context.update({
            'features': images,
            'instance': instance,
            })

        return context
plugin_pool.register_plugin(BannerRotatorPlugin)


class CarouselPlugin(CMSPluginBase):
    """
    Plugin to render a feature slider
    """
    model = CarouselPluginModel
    name = _("Carousel")
    render_template = "bannershow/bannerCarousel.html"
    text_enabled = True
    admin_preview = False
    module = "Bannershow"
    fieldsets = (
        (_('General'), {
            'fields': (
                'title',
                'images',
                ('width', 'height', ),
                'skin',
                ('animationTime', 'autoPlay', ),
            ),
        }),
        (_('Display'), {
            'fields': (
                'resizeImages',
                ('responsive', 'responsiveRelativeToBrowser', ),
                'numberOfVisibleItems',
                'elementsHorizontalSpacing',
                'elementsVerticalSpacing',
                'verticalAdjustment',
                'playMovieMarginTop',
            ),
        }),
        (_('Controls'), {
            'fields': (
                'enableTouchScreen',
                'showCircleTimer',
                'arrows',
                'pause_on_hover',
                'nextPrevMarginTop',
                'bottomNavMarginBottom',
            ),
        }),
    )

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
            })

        return context
plugin_pool.register_plugin(CarouselPlugin)
