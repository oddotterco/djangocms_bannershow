"""
Admin interfaces
"""

from django.contrib import admin
from models import FeatureImage

class FeatureImageAdmin(admin.ModelAdmin):
    list_display = (
        'image',
        'link',
        'caption',
        )
    list_editable = [
        'link',
        'caption',
        ]
    search_fields = [
        'caption',
        ]
admin.site.register(FeatureImage, FeatureImageAdmin)